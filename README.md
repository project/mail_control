CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * REQUIREMENTS
 * INSTALLATION
 * CONFIGURATION
 * MAINTAINER


INTRODUCTION
------------
Have 100% control over the default mails sent by Drupal 8 out of the box.

This small module extends original Drupal 8 account settings page with 
the ability to turn ON/OFF all standard mails. By default Drupal 8 
provides no option to disable welcome emails, account cancellation 
confirmation or password reset.
This can be useful when you don't want to send those emails, or you are 
sending them through some external services 
or just for development purposes, etc.
You can access these settings under admin/config/people/accounts.
The module provide only interface modifications. 
The underlying functionality is already there by default in Drupal 8.


REQUIREMENTS
------------

This module requires the following modules:
drupal:user


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
------------

* Configure user account settings in /admin/config/people/accounts

MAINTAINER:
 * Atul Dubey (soaratul) - https://www.drupal.org/u/soaratul
